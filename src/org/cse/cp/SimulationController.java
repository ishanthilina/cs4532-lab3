package org.cse.cp;

import java.util.concurrent.Semaphore;

public class SimulationController {

    static Semaphore multiplex;
    static Semaphore mutex;
    static Semaphore bus;
    static Semaphore allAboard;
    public static int riders;
    public static int inTheBus;

    public SimulationController() {

    }

    public static void main(String[] args) {

        multiplex = new Semaphore(50);
        mutex = new Semaphore(1);
        bus = new Semaphore(0);
        allAboard = new Semaphore(0);
        riders = 0;
        inTheBus = 0;

        PassengerFactory passengerFactory = new PassengerFactory();
        Thread passengerScheduler = new Thread(passengerFactory);
        passengerScheduler.start();

        BusScheduler scheduler = new BusScheduler();
        Thread busScheduler = new Thread(scheduler);
        busScheduler.start();


    }
}
