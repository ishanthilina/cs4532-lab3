package org.cse.cp;

public class BusScheduler implements Runnable {

    int busCount = 0;

    public BusScheduler() {
    }

    @Override
    public void run() {
        //send busses forever
        while (true) {
            Thread busThread = new Thread(new Bus(busCount++));
            busThread.start();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
