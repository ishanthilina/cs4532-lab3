package org.cse.cp;


public class Passenger implements Runnable {


    int myIndex = 0;

    public Passenger(int myIndex) {
        this.myIndex = myIndex;
    }


    @Override
    public void run() {
        System.out.println("Passsenger " + myIndex + " trying");

        try {

            //reserve a place for the passenger in the queue
            SimulationController.multiplex.acquire();
            //let others know I am waiting in the queue
            SimulationController.mutex.acquire();
            SimulationController.riders += 1;
            SimulationController.mutex.release();

            //wait for the bus
            SimulationController.bus.acquire();

            ///This part is executed when a bus arrives
            boardBus();


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void boardBus() {
        System.out.println("Passenger " + myIndex + " boarded to bus");
        /**
         * No need to protect the following section with a mutex since only a single passenger can access
         * the section because only a single passenger is released by the bus when the bus arrives.
         */
        SimulationController.riders -= 1;
        //increment the passenger count
        SimulationController.inTheBus += 1;
        //if all the passengers have got in the bus
        if (SimulationController.riders == 0) {
            SimulationController.allAboard.release();
            //free the queue space
            SimulationController.multiplex.release();
        }

        else {
            //let another passenger get in the bus
            SimulationController.bus.release();
            //free the queue space
            SimulationController.multiplex.release();
        }
    }
}
