package org.cse.cp;


public class PassengerFactory implements Runnable {

    int passengerCount = 0;

    public PassengerFactory() {
    }

    @Override
    public void run() {
        while (true) {
            try {
                for (int i = 0; i < 50; i++) {
                    Thread passenger = new Thread(new Passenger(passengerCount++));
                    passenger.start();
                    Thread.sleep(150);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
