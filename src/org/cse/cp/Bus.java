package org.cse.cp;


public class Bus implements Runnable {

    int myIndex;

    public Bus(int myIndex) {
        this.myIndex = myIndex;
    }

    @Override
    public void run() {
        try {
            System.out.println("Bus " + myIndex + " started");
            //prevent new comers from boarding the bus
            SimulationController.mutex.acquire();

            //let the existing passengers in
            if (SimulationController.riders > 0) {
                //let a passenger know that the bus has arrived
                System.out.println("Bus " + myIndex + " notifying passengers");
                SimulationController.bus.release();
                //check if everybody has boarded
                SimulationController.allAboard.acquire();

            }

            System.out.println("Bus " + myIndex + " departed with " + SimulationController.inTheBus + " passengers.");
            SimulationController.inTheBus = 0;
            //let new comers to join the queue
            SimulationController.mutex.release();



        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
